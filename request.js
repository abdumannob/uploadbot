// // request.js
// const axios = require("axios");

// async function downLoaderMethod(insta_url) {
//   try {
//     const options = {
//       method: "GET",
//       url: "https://instagram-downloader-download-instagram-videos-stories1.p.rapidapi.com/",
//       params: {
//         url: insta_url,
//       },
//       headers: {
//         "X-RapidAPI-Key": "9d13524791mshc8ccac4fb26dd38p17689cjsn67d80e938e6b",
//         "X-RapidAPI-Host":
//           "instagram-downloader-download-instagram-videos-stories1.p.rapidapi.com",
//       },
//     };

//     const response = await axios.request(options);
//     const result = {
//       videoUrl: response.data[0].url,
//       title: response.data[0].title,
//     };

//     return result;
//   } catch (error) {
//     console.error(error);
//   }
// }

// module.exports = {
//   downLoaderMethod,
// };
const axios = require("axios");

async function downLoaderMethod(insta_url) {
  try {
    const response = await axios.get(
      `https://instagram-downloader-download-instagram-videos-stories1.p.rapidapi.com/?url=${insta_url}`,
      {
        headers: {
          "X-RapidAPI-Key":
            "9d13524791mshc8ccac4fb26dd38p17689cjsn67d80e938e6b",
          "X-RapidAPI-Host":
            "instagram-downloader-download-instagram-videos-stories1.p.rapidapi.com",
        },
      }
    );

    if (response.data && response.data.length > 0) {
      return {
        videoUrl: response.data[0].url,
        title: response.data[0].title,
      };
    } else {
      return null;
    }
  } catch (error) {
    console.error(error);
    return null;
  }
}

module.exports = {
  downLoaderMethod,
};

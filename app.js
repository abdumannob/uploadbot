// // app.js
// const Telegrambot = require("node-telegram-bot-api");
// const TOKEN = "7054586689:AAGFOxO4wdRMsQrggBJCzHgJDB9W_gebzW4";
// const { downLoaderMethod } = require("./request");

// const bot = new Telegrambot(TOKEN, { polling: true });

// bot.on("message", async (message) => {
//   try {
//     const chatId = message.chat.id;
//     const name = message.from.first_name;
//     if (message.text === "/start") {
//       await bot.sendMessage(
//         chatId,
//         `Assalomu alaykum <b>${name}</b>, botimizga xush kelibsiz.\nBotga instagramdan video link yuboring men sizga yuklab beraman`,
//         { parse_mode: "HTML" }
//       );
//     }
//     const getVideo = await downLoaderMethod(message.text);
//     await bot.sendVideo(chatId, getVideo.videoUrl, {
//       caption: getVideo.title + "\n Telegram @Fullstaking",
//     });
//   } catch (error) {
//     console.log(error + "");
//   }
// });

// bot.onText(/\/help/, (msg) => {
//   const chatId = msg.chat.id;
//   bot.sendMessage(chatId, "Nima yordam kerak!");
// });
const Telegrambot = require("node-telegram-bot-api");
const TOKEN = "7054586689:AAGFOxO4wdRMsQrggBJCzHgJDB9W_gebzW4";
const { downLoaderMethod } = require("./request");

const bot = new Telegrambot(TOKEN, { polling: true });

bot.onText(/\/start/, async (message) => {
  try {
    const chatId = message.chat.id;
    const name = message.from.first_name;
    if (message.text === "/start") {
      await bot.sendMessage(
        chatId,
        `Assalomu alaykum <b>${name}</b>, botimizga xush kelibsiz.\nBotga instagramdan video link yuboring men sizga yuklab beraman`,
        { parse_mode: "HTML" }
      );
    }
  } catch (error) {
    console.log(error + "");
  }
});

bot.on("message", async (message) => {
  try {
    const chatId = message.chat.id;
    const getVideo = await downLoaderMethod(message.text);
    if (getVideo && getVideo.videoUrl) {
      await bot.sendVideo(chatId, getVideo.videoUrl, {
        caption: getVideo.title + "\n Telegram @Fullstaking",
      });
    } else {
      await bot.sendMessage(chatId, "Video topilmadi!");
    }
  } catch (error) {
    console.log(error + "");
  }
});

bot.onText(/\/help/, (msg) => {
  const chatId = msg.chat.id;
  bot.sendMessage(chatId, "Nima yordam kerak!");
});
